﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    class Lookback : Option
    {
        public override double[,] Getprice(double[,] Path, int Trials, int Steps, double Tenor,double S, double K, double R, double vol,double Rebate,double Barrier)
        {
            //Pricing Lookback option is also almost the same as European option pricing
            //The difference is that for each path, we need to find the maximun or the minimun
            //then the payoff is Max-K for call option and K-Min for put option


            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================
            if (IO.Antithetic == true)//with antithetic
            {
                if (IO.CV == true)
                {
                    if (IO.Call == true)//anti&CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        //for call option, we instan. a max array
                        double[,] max = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                            max[a, 0] = S;
                            max[(2 * Trials) - 1 - a, 0] = S;
                        }
                        //look for max price of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                max[a, 0] = Math.Max(max[a, 0], Path[a, b]);
                                max[(2 * Trials) - 1 - a, 0] = Math.Max(max[(2 * Trials) - 1 - a, 0], Path[(2 * Trials) - 1 - a, b]);
                            }
                        }
                        //calculate CT
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * (Math.Max(max[a, 0] - K, 0) - cv[a, 0] + Math.Max(max[(2 * Trials) - 1 - a, 0] - K, 0) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                    else//anti&CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        //for put option, we instan. a min array
                        double[,] min = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                            min[a, 0] = S;
                            min[(2 * Trials) - 1 - a, 0] = S;
                        }
                        //look for min of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                min[a, 0] = Math.Min(min[a, 0], Path[a, b]);
                                min[(2 * Trials) - 1 - a, 0] = Math.Min(min[(2 * Trials) - 1 - a, 0], Path[(2 * Trials) - 1 - a, b]);
                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * (Math.Max(K - min[a, 0], 0) - cv[a, 0] + Math.Max(K - min[(2 * Trials) - 1 - a, 0], 0) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                }
                else
                {
                    if (IO.Call == true)//anti&call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        //for call option, we instan. a max array
                        double[,] max = new double[2 * Trials, 1];
                        for (int a = 0; a <= Trials - 1; a++)
                        {

                            max[a, 0] = S;
                            max[(2 * Trials) - 1 - a, 0] = S;
                        }
                        //look for max price of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                max[a, 0] = Math.Max(max[a, 0], Path[a, b]);
                                max[(2 * Trials) - 1 - a, 0] = Math.Max(max[(2 * Trials) - 1 - a, 0], Path[(2 * Trials) - 1 - a, b]);
                            }
                        }

                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(max[a, 0] - K, 0);

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(max[a - 1, 0] - K, 0) * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                    else//anti&put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        //for put option, we instan. a min array
                        double[,] min = new double[2 * Trials, 1];
                        for (int a = 0; a <= Trials - 1; a++)
                        {

                            min[a, 0] = S;
                            min[(2 * Trials) - 1 - a, 0] = S;
                        }
                        //look for min of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                min[a, 0] = Math.Min(min[a, 0], Path[a, b]);
                                min[(2 * Trials) - 1 - a, 0] = Math.Min(min[(2 * Trials) - 1 - a, 0], Path[(2 * Trials) - 1 - a, b]);
                            }
                        }

                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(K - min[a, 0], 0);

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(K - min[a - 1, 0], 0) * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================





            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================
            else//without antithetic
            {
                if (IO.CV == true)
                {
                    if (IO.Call == true)//CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        //for call option, we instan. a max array
                        double[,] max = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;

                            max[a, 0] = S;

                        }
                        //look for max price of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                max[a, 0] = Math.Max(max[a, 0], Path[a, b]);

                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = Math.Max(max[a, 0] - K, 0) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }
                    else//CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        //for put option, we instan. a min array
                        double[,] min = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;

                            min[a, 0] = S;

                        }
                        //look for min of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                min[a, 0] = Math.Min(min[a, 0], Path[a, b]);

                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = Math.Max(K - min[a, 0], 0) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }

                }
                else
                {
                    if (IO.Call == true)//call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        //for call option, we instan. a max array
                        double[,] max = new double[Trials, 1];

                        for (int a = 0; a <= Trials - 1; a++)
                        {


                            max[a, 0] = S;

                        }
                        //look for max price of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                max[a, 0] = Math.Max(max[a, 0], Path[a, b]);

                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(max[a, 0] - K, 0);

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(max[a - 1, 0] - K, 0) * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                    else//put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        //for put option, we instan. a min array
                        double[,] min = new double[Trials, 1];

                        for (int a = 0; a <= Trials - 1; a++)
                        {


                            min[a, 0] = S;

                        }
                        //look for min of each path
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                min[a, 0] = Math.Min(min[a, 0], Path[a, b]);

                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(K - min[a, 0], 0);

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(K - min[a - 1, 0], 0) * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================







            //return base.Getprice(Path, Trials, Steps, Tenor, K, R, vol);
        }
    }
}
