﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    class Digital:Option
    {
        public override double[,] Getprice(double[,] Path, int Trials, int Steps, double Tenor, double S, double K, double R, double vol, double Rebate, double Barrier)
        {
            //For digital option, we need to know whether an path is in the money or not based its final price, so we instan. a In_the_money array to decide
            //When it is in the money, the corresponding lattice is 1, otherwise it is 0
            //and then payoff is rebate * In_the_money. In this way, if it is in the money, payoff is rebate, if not, payoff is 0

            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================
            if (IO.Antithetic == true)//with antithetic
            {
                double[] In_the_money = new double[2 * Trials];

                if (IO.CV == true)
                {
                    if (IO.Call == true)//anti&CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);
                        //Dicide in or out the money
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            if (Path[a, Steps] > K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * ((In_the_money[a]*Rebate) - cv[a, 0] + (In_the_money[(2*Trials)-1-a]*Rebate) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                    else//anti&CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);
                        //Dicide in or out the money
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            if (Path[a, Steps] < K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * ((In_the_money[a] * Rebate) - cv[a, 0] + (In_the_money[(2 * Trials) - 1 - a] * Rebate) - cv[(2 * Trials) - 1 - a, 0]);
                        }


                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                }
                else
                {
                    if (IO.Call == true)//anti&call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        //Dicide in or out the money
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            if (Path[a, Steps] > K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }

                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += In_the_money[a] * Rebate;

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = In_the_money[a-1]*Rebate * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                    else//anti&put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        //Dicide in or out the money
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            if (Path[a, Steps] < K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += In_the_money[a] * Rebate;

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = In_the_money[a-1] * Rebate * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================





            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================
            else//without antithetic
            {
                double[] In_the_money = new double[Trials];
                if (IO.CV == true)
                {
                    if (IO.Call == true)//CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);
                        //Dicide in or out the money
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            if (Path[a, Steps] > K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = (In_the_money[a]*Rebate) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }
                    else//CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);
                        //Dicide in or out the money
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            if (Path[a, Steps] < K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = (In_the_money[a] * Rebate) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }

                }
                else
                {
                    if (IO.Call == true)//call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        //Dicide in or out the money
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            if (Path[a, Steps] > K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += In_the_money[a]*Rebate;

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = In_the_money[a-1]*Rebate * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                    else//put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        //Dicide in or out the money
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            if (Path[a, Steps] < K)
                            {
                                In_the_money[a] = 1;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                        for (int a = 0; a < Trials; a++)
                        {
                            sum += In_the_money[a] * Rebate;

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = In_the_money[a-1] * Rebate * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================

        }
    }
}
