﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    class Range:Option
    {
        public override double[,] Getprice(double[,] Path, int Trials, int Steps, double Tenor, double S, double K, double R, double vol,double Rebate,double Barrier)
        {
            //Range option doesn't care call or put, we just need the maximun and the minimun to calculate payoff
            if (IO.Antithetic == true)
            {
                if (IO.CV == true)
                {
                    double[,] CT = new double[Trials, 1];
                    double dt = Tenor / Convert.ToDouble(Steps);
                    double[,] cv = new double[2 * Trials, 1];
                    double delta_1, delta_2;
                    double cont_t;
                    double erddt = Math.Exp(R * dt);
                    double[] Range = new double[2 * Trials];
                    //loof for range
                    for(int a = 0; a <= (2 * Trials) - 1; a++)
                    {
                        double max = S;
                        double min = S;
                        for(int b = 0; b <= Steps; b++)
                        {
                            if (Path[a, b] > max)
                            {
                                max = Path[a, b];
                            }
                            if (Path[a, b] < min)
                            {
                                min = Path[a, b];
                            }
                        }
                        Range[a] = max - min;
                    }

                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        cv[a, 0] = 0;
                        cv[(2 * Trials) - 1 - a, 0] = 0;
                    }

                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            cont_t = (b - 1) * dt;
                            delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                            delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                            //calculate cv
                            cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                        }
                        CT[a, 0] = 0.5 * (Range[a] - cv[a, 0] + Range[(2 * Trials) - 1 - a] - cv[(2 * Trials) - 1 - a, 0]);
                    }
                    //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                    double[,] sum_ct = new double[1, 3];
                    sum_ct[0, 0] = 0;
                    sum_ct[0, 1] = 0;
                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        sum_ct[0, 0] += CT[a, 0];
                        sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                    }
                    sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                    return sum_ct;
                }
                else
                {
                    double sum = 0;
                    double[,] Result = new double[1, (2 * Trials) + 1];
                    double[] Range = new double[2 * Trials];
                    //loof for range
                    for (int a = 0; a <= (2 * Trials) - 1; a++)
                    {
                        double max = S;
                        double min = S;
                        for (int b = 0; b <= Steps; b++)
                        {
                            if (Path[a, b] > max)
                            {
                                max = Path[a, b];
                            }
                            if (Path[a, b] < min)
                            {
                                min = Path[a, b];
                            }
                        }
                        Range[a] = max - min;
                    }
                    for (int a = 0; a <= (2 * Trials) - 1; a++)
                    {
                        sum += Range[a];

                    }
                    Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                    for (int a = 1; a <= (2 * Trials); a++)
                    {
                        Result[0, a] = Range[a-1] * Math.Exp(-(R * Tenor));
                    }
                    return Result;
                }
            }
            else
            {
                if (IO.CV == true)
                {
                    double[,] CT = new double[Trials, 1];
                    double dt = Tenor / Convert.ToDouble(Steps);
                    double[,] cv = new double[Trials, 1];
                    double delta_1;
                    double cont_t;
                    double erddt = Math.Exp(R * dt);
                    double[] Range = new double[Trials];
                    //loof for range
                    for (int a = 0; a <=  Trials - 1; a++)
                    {
                        double max = S;
                        double min = S;
                        for (int b = 0; b <= Steps; b++)
                        {
                            if (Path[a, b] > max)
                            {
                                max = Path[a, b];
                            }
                            if (Path[a, b] < min)
                            {
                                min = Path[a, b];
                            }
                        }
                        Range[a] = max - min;
                    }
                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        cv[a, 0] = 0;
                    }

                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            cont_t = (b - 1) * dt;
                            delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                            //calculate cv
                            cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                        }
                        CT[a, 0] = Range[a] - cv[a, 0];
                    }

                    //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                    double[,] sum_ct = new double[1, 3];
                    sum_ct[0, 0] = 0;
                    sum_ct[0, 1] = 0;
                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        sum_ct[0, 0] += CT[a, 0];
                        sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                    }
                    sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                    return sum_ct;
                }
                else
                {
                    double sum = 0;
                    double[,] Result = new double[1, Trials + 1];
                    double[] Range = new double[2 * Trials];
                    //loof for range
                    for (int a = 0; a <=  Trials - 1; a++)
                    {
                        double max = S;
                        double min = S;
                        for (int b = 0; b <= Steps; b++)
                        {
                            if (Path[a, b] > max)
                            {
                                max = Path[a, b];
                            }
                            if (Path[a, b] < min)
                            {
                                min = Path[a, b];
                            }
                        }
                        Range[a] = max - min;
                    }
                    for (int a = 0; a <= Trials - 1; a++)
                    {
                        sum += Range[a];

                    }

                    Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                    for (int a = 1; a <= Trials; a++)
                    {
                        Result[0, a] = Range[a-1] * Math.Exp(-(R * Tenor));
                    }

                    return Result;
                }
            }
            //return base.Getprice(Path, Trials, Steps, Tenor, S, K, R, vol);
        }
    }
}
