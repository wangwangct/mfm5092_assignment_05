﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    //In general, there are 4 steps in option pricing,
    //generate random matrix, generate path, calculate price, calculate greeks values and SE.
    //All the other different kind of option class inherite from this class, and every other class overides the Getprice method
    //In other words, the only difference between each class is Getprice method.
    //Option class itself prices European option



    class Option
    {
        Simulator sim = new Simulator();
        Stopwatch watch = new Stopwatch();

        public void schedular()
        {
            if (IO.CV == true)//sse control variate
            {


                watch.Start();
                //instan. a random matrix of the size
                double[,] r = new double[IO.Trials, IO.Steps];
                //generate random matrix
                r = sim.random();

                //generate path matrices with different inputs
                double[,] Path = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_ds = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_minus_ds = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_dv = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility),IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_minus_dv = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility),IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_dt = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_dr = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate + (0.001 * IO.Risk_free_rate), IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_minus_dr = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate - (0.001 * IO.Risk_free_rate), IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                //calculate option price with different inputs
                double[,] Price = Getprice(Path, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying,IO.Strike_Price, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier);
                double True_Price = Price[0, 2];
                Program.increase(1);
                double Price_plus_ds = Getprice(Path_plus_ds, IO.Trials, IO.Steps, IO.Tenor,IO.Underlying*1.001, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier)[0, 2];
                double Price_minus_ds = Getprice(Path_minus_ds, IO.Trials, IO.Steps, IO.Tenor,IO.Underlying*0.999, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier)[0, 2];
                Program.increase(1);
                double Price_plus_dv = Getprice(Path_plus_dv, IO.Trials, IO.Steps, IO.Tenor,IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility),IO.Rebate,IO.Barrier)[0, 2];
                double Price_minus_dv = Getprice(Path_minus_dv, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying,IO.Strike_Price, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility),IO.Rebate,IO.Barrier)[0, 2];
                Program.increase(1);
                double Price_plus_dt = Getprice(Path_plus_dt, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)),IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier)[0, 2];
                double Price_plus_dr = Getprice(Path_plus_dr, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying,IO.Strike_Price, IO.Risk_free_rate + (0.001 * IO.Risk_free_rate), IO.Volatility,IO.Rebate,IO.Barrier)[0, 2];
                double Price_minus_dr = Getprice(Path_minus_dr, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying,IO.Strike_Price, IO.Risk_free_rate - (0.001 * IO.Risk_free_rate), IO.Volatility,IO.Rebate,IO.Barrier)[0, 2];
                Program.increase(1);
                //transfer results into IO class
                IO.O_price = True_Price;
                IO.O_se = SE(Price);
                IO.O_delta = Delta(Price_plus_ds, Price_minus_ds);
                IO.O_gamma = Gamma(Price_plus_ds, True_Price, Price_minus_ds);
                IO.O_vega = Vega(Price_plus_dv, Price_minus_dv);
                IO.O_theta = Theta(Price_plus_dt, True_Price);
                IO.O_rho = Rho(Price_plus_dr, Price_minus_dr);
                Program.increase(1);

                watch.Stop();
                IO.Timer = watch.Elapsed.Hours.ToString() + ":"
                    + watch.Elapsed.Minutes.ToString() + ":"
                    + watch.Elapsed.Seconds.ToString() + ":"
                    + watch.Elapsed.Milliseconds.ToString();
                Program.finished();

                GC.Collect();
            }
            else//do no use control variate
            {
                watch.Start();
                //instan. a random matrix of the size
                double[,] r = new double[IO.Trials, IO.Steps];
                r = sim.random();


                double[,] Path = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate, IO.Volatility,IO.Rebate,IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_ds = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying * 1.001, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Path_minus_ds = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying * 0.999, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_dv = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility), IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Path_minus_dv = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility), IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_dt = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Path_plus_dr = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate * 1.001, IO.Volatility, IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Path_minus_dr = sim.Getpath(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Risk_free_rate * 0.999, IO.Volatility, IO.Rebate, IO.Barrier);
                Program.increase(1);
                double[,] Price = Getprice(Path, IO.Trials, IO.Steps, IO.Tenor,IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier);
                double True_Price = Price[0, 0];
                Program.increase(1);
                double Price_plus_ds = Getprice(Path_plus_ds, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying*1.001,IO.Strike_Price, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier)[0, 0];
                double Price_minus_ds = Getprice(Path_minus_ds, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying*0.999,IO.Strike_Price, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier)[0, 0];
                Program.increase(1);
                double Price_plus_dv = Getprice(Path_plus_dv, IO.Trials, IO.Steps, IO.Tenor,IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility * 1.001, IO.Rebate, IO.Barrier)[0, 0];
                double Price_minus_dv = Getprice(Path_minus_dv, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility * 0.999, IO.Rebate, IO.Barrier)[0, 0];
                Program.increase(1);
                double Price_plus_dt = Getprice(Path_plus_dt, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)),IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility, IO.Rebate, IO.Barrier)[0, 0];
                double Price_plus_dr = Getprice(Path_plus_dr, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying,IO.Strike_Price, IO.Risk_free_rate * 1.001, IO.Volatility, IO.Rebate, IO.Barrier)[0, 0];
                double Price_minus_dr = Getprice(Path_minus_dr, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying,IO.Strike_Price, IO.Risk_free_rate * 0.999, IO.Volatility, IO.Rebate, IO.Barrier)[0, 0];
                Program.increase(1);
                IO.O_price = True_Price;
                IO.O_se = SE(Price);
                IO.O_delta = Delta(Price_plus_ds, Price_minus_ds);
                IO.O_gamma = Gamma(Price_plus_ds, True_Price, Price_minus_ds);
                IO.O_vega = Vega(Price_plus_dv, Price_minus_dv);
                IO.O_theta = Theta(Price_plus_dt, True_Price);
                IO.O_rho = Rho(Price_plus_dr, Price_minus_dr);
                Program.increase(1);

                watch.Stop();
                IO.Timer = watch.Elapsed.Hours.ToString() + ":"
                    + watch.Elapsed.Minutes.ToString() + ":"
                    + watch.Elapsed.Seconds.ToString() + ":"
                    + watch.Elapsed.Milliseconds.ToString();
                Program.finished();

                GC.Collect();
            }

        }

        public virtual double[,] Getprice(double[,] Path, int Trials, int Steps, double Tenor, double S,double K, double R, double vol,double Rebate,double Barrier)
        {
            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================
            if (IO.Antithetic == true)//with antithetic
            {
                if (IO.CV == true)
                {
                    if (IO.Call == true)//anti&CV&call
                    {
                        //set CT,cv as array, every path has a cv value, and every two paths have a CT value 
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        //set cv as 0
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }
                        //calculate CT
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * (Math.Max(Path[a, Steps] - K, 0) - cv[a, 0] + Math.Max(Path[(2 * Trials) - 1 - a, Steps] - K, 0) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                    else//anti&CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * (Math.Max(K - Path[a, Steps], 0) - cv[a, 0] + Math.Max(K - Path[(2 * Trials) - 1 - a, Steps], 0) - cv[(2 * Trials) - 1 - a, 0]);
                        }


                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                }
                else
                {
                    if (IO.Call == true)//anti&call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(Path[a, Steps] - K, 0);

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(Path[a - 1, Steps] - K, 0) * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                    else//anti&put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(K - Path[a, Steps], 0);

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(K - Path[a - 1, Steps], 0) * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================





            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================
            else//without antithetic
            {
                if (IO.CV == true)
                {
                    if (IO.Call == true)//CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = Math.Max(Path[a, Steps] - K, 0) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }
                    else//CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = Math.Max(K - Path[a, Steps], 0) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }

                }
                else
                {
                    if (IO.Call == true)//call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(Path[a, Steps] - K, 0);

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(Path[a - 1, Steps] - K, 0) * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                    else//put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        for (int a = 0; a < Trials; a++)
                        {
                            sum += Math.Max(K - Path[a, Steps], 0);

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(K - Path[a - 1, Steps], 0) * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================

        }

        //The following part calculate greeks values and SE
        public double Delta(double price_plus_ds, double price_minus_ds)
        {
            double result = (price_plus_ds - price_minus_ds) / (0.002 * IO.Underlying);
            return result;
        }
        public double Gamma(double price_plus_ds, double price, double price_minus_ds)
        {
            double result = (price_plus_ds - (2 * price) + price_minus_ds) / Math.Pow(0.001 * IO.Underlying, 2);
            return result;
        }
        public double Vega(double price_plus_dv, double price_minus_dv)
        {
            double result = (price_plus_dv - price_minus_dv) / (0.002 * IO.Volatility);
            return result;
        }
        public double Theta(double price_plus_dt, double price)
        {
            double result = -(price_plus_dt - price) / (IO.Tenor / Convert.ToDouble(IO.Steps));
            return result;
        }
        public double Rho(double price_plus_dr, double price_minus_dr)
        {
            double result = (price_plus_dr - price_minus_dr) / (0.002 * IO.Risk_free_rate);
            return result;
        }

        public double SE(double[,] Price)
        {
            if (IO.CV == true)//if use CV, no matter user uses antithetic, the calculation of SE is the same
            {


                double SD = Math.Sqrt((Price[0, 1] - (Price[0, 0] * Price[0, 0] / IO.Trials)) * Math.Exp(-2 * IO.Risk_free_rate * IO.Tenor) / (IO.Trials - 1));
                double SE = SD / Math.Sqrt(IO.Trials);
                return SE;
            }
            else//IO.CV==false
            {
                if (IO.Antithetic == true)
                {
                    double[,] ave = new double[1, IO.Trials];
                    double var = 0;
                    for (int a = 0; a <= IO.Trials - 1; a++) //calculate the average of each pair of antithetic prices
                    {
                        ave[0, a] = (Price[0, a + 1] + Price[0, (2 * IO.Trials) - a]) / 2;
                    }
                    for (int a = 0; a <= IO.Trials - 1; a++)//calculate variance
                    {
                        var += Math.Pow(ave[0, a] - Price[0, 0], 2);
                    }
                    var = var / (IO.Trials - 1);
                    //calculate standard error 
                    double SE = Math.Sqrt(var / IO.Trials);
                    return SE;
                }
                else
                {
                    double sum = 0;
                    for (int a = 1; a <= IO.Trials; a++)
                    {
                        sum += Math.Pow(Price[0, a] - Price[0, 0], 2);
                    }
                    double SD = Math.Sqrt((sum) / (IO.Trials - 1));
                    double SE = SD / Math.Sqrt(IO.Trials);
                    return SE;
                }
            }
        }
    }
}
