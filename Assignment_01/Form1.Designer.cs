﻿namespace Assignment_01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radioButton_euro_call = new System.Windows.Forms.RadioButton();
            this.radioButton_euro_put = new System.Windows.Forms.RadioButton();
            this.lable_steps = new System.Windows.Forms.Label();
            this.label_S = new System.Windows.Forms.Label();
            this.label_K = new System.Windows.Forms.Label();
            this.label_r = new System.Windows.Forms.Label();
            this.label_vol = new System.Windows.Forms.Label();
            this.textBox_trials = new System.Windows.Forms.TextBox();
            this.textBox_steps = new System.Windows.Forms.TextBox();
            this.textBox_tenor = new System.Windows.Forms.TextBox();
            this.textBox_S = new System.Windows.Forms.TextBox();
            this.textBox_K = new System.Windows.Forms.TextBox();
            this.textBox_r = new System.Windows.Forms.TextBox();
            this.textBox_vol = new System.Windows.Forms.TextBox();
            this.button_go = new System.Windows.Forms.Button();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.label_price = new System.Windows.Forms.Label();
            this.textBox_delta = new System.Windows.Forms.TextBox();
            this.label_delta = new System.Windows.Forms.Label();
            this.textBox_gamma = new System.Windows.Forms.TextBox();
            this.textBox_vega = new System.Windows.Forms.TextBox();
            this.textBox_theta = new System.Windows.Forms.TextBox();
            this.textBox_rho = new System.Windows.Forms.TextBox();
            this.textBox_se = new System.Windows.Forms.TextBox();
            this.label_gamma = new System.Windows.Forms.Label();
            this.label_vega = new System.Windows.Forms.Label();
            this.label_theta = new System.Windows.Forms.Label();
            this.label_rho = new System.Windows.Forms.Label();
            this.label_se = new System.Windows.Forms.Label();
            this.checkBox_antithetic = new System.Windows.Forms.CheckBox();
            this.label_time = new System.Windows.Forms.Label();
            this.textBox_time = new System.Windows.Forms.TextBox();
            this.checkBox_vc = new System.Windows.Forms.CheckBox();
            this.lable_trials = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label_cores = new System.Windows.Forms.Label();
            this.button_reset = new System.Windows.Forms.Button();
            this.checkBox_MT = new System.Windows.Forms.CheckBox();
            this.label_tenor = new System.Windows.Forms.Label();
            this.radioButton_asian_call = new System.Windows.Forms.RadioButton();
            this.radioButton_asian_put = new System.Windows.Forms.RadioButton();
            this.label_euro = new System.Windows.Forms.Label();
            this.label_asian = new System.Windows.Forms.Label();
            this.radioButton_range = new System.Windows.Forms.RadioButton();
            this.label_range = new System.Windows.Forms.Label();
            this.label_lookback = new System.Windows.Forms.Label();
            this.radioButton_lookback_put = new System.Windows.Forms.RadioButton();
            this.radioButton_lookback_call = new System.Windows.Forms.RadioButton();
            this.label_digital = new System.Windows.Forms.Label();
            this.radioButton_digital_call = new System.Windows.Forms.RadioButton();
            this.radioButton_digital_put = new System.Windows.Forms.RadioButton();
            this.radioButton_barrier_put = new System.Windows.Forms.RadioButton();
            this.textBox_barrier = new System.Windows.Forms.TextBox();
            this.textBox_rebate = new System.Windows.Forms.TextBox();
            this.radioButton_barrier_up_in = new System.Windows.Forms.RadioButton();
            this.radioButton_barrier_up_out = new System.Windows.Forms.RadioButton();
            this.radioButton_barrier_down_out = new System.Windows.Forms.RadioButton();
            this.radioButton_barrier_call = new System.Windows.Forms.RadioButton();
            this.radioButton_barrier_down_in = new System.Windows.Forms.RadioButton();
            this.label_barrier = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButton_euro_call
            // 
            this.radioButton_euro_call.Checked = true;
            this.radioButton_euro_call.Location = new System.Drawing.Point(106, 305);
            this.radioButton_euro_call.Name = "radioButton_euro_call";
            this.radioButton_euro_call.Size = new System.Drawing.Size(42, 17);
            this.radioButton_euro_call.TabIndex = 14;
            this.radioButton_euro_call.TabStop = true;
            this.radioButton_euro_call.Text = "Call";
            this.radioButton_euro_call.UseVisualStyleBackColor = true;
            // 
            // radioButton_euro_put
            // 
            this.radioButton_euro_put.Location = new System.Drawing.Point(205, 305);
            this.radioButton_euro_put.Name = "radioButton_euro_put";
            this.radioButton_euro_put.Size = new System.Drawing.Size(41, 17);
            this.radioButton_euro_put.TabIndex = 15;
            this.radioButton_euro_put.Text = "Put";
            this.radioButton_euro_put.UseVisualStyleBackColor = true;
            // 
            // lable_steps
            // 
            this.lable_steps.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_steps.Location = new System.Drawing.Point(14, 42);
            this.lable_steps.Name = "lable_steps";
            this.lable_steps.Size = new System.Drawing.Size(46, 18);
            this.lable_steps.TabIndex = 1;
            this.lable_steps.Text = "Steps";
            // 
            // label_S
            // 
            this.label_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_S.Location = new System.Drawing.Point(13, 99);
            this.label_S.Name = "label_S";
            this.label_S.Size = new System.Drawing.Size(115, 18);
            this.label_S.TabIndex = 3;
            this.label_S.Text = "Underlying Price";
            // 
            // label_K
            // 
            this.label_K.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_K.Location = new System.Drawing.Point(13, 129);
            this.label_K.Name = "label_K";
            this.label_K.Size = new System.Drawing.Size(84, 18);
            this.label_K.TabIndex = 4;
            this.label_K.Text = "Strike Price";
            // 
            // label_r
            // 
            this.label_r.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_r.Location = new System.Drawing.Point(13, 159);
            this.label_r.Name = "label_r";
            this.label_r.Size = new System.Drawing.Size(110, 18);
            this.label_r.TabIndex = 5;
            this.label_r.Text = "Risk_free  Rate";
            // 
            // label_vol
            // 
            this.label_vol.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vol.Location = new System.Drawing.Point(13, 189);
            this.label_vol.Name = "label_vol";
            this.label_vol.Size = new System.Drawing.Size(61, 18);
            this.label_vol.TabIndex = 6;
            this.label_vol.Text = "Volatility";
            // 
            // textBox_trials
            // 
            this.textBox_trials.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_trials.Location = new System.Drawing.Point(146, 12);
            this.textBox_trials.Name = "textBox_trials";
            this.textBox_trials.Size = new System.Drawing.Size(100, 24);
            this.textBox_trials.TabIndex = 7;
            this.textBox_trials.Text = "10000";
            this.textBox_trials.Leave += new System.EventHandler(this.textBox_trials_Leave);
            // 
            // textBox_steps
            // 
            this.textBox_steps.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_steps.Location = new System.Drawing.Point(146, 39);
            this.textBox_steps.Name = "textBox_steps";
            this.textBox_steps.Size = new System.Drawing.Size(100, 24);
            this.textBox_steps.TabIndex = 8;
            this.textBox_steps.Text = "100";
            this.textBox_steps.Leave += new System.EventHandler(this.textBox_steps_Leave);
            // 
            // textBox_tenor
            // 
            this.textBox_tenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_tenor.Location = new System.Drawing.Point(146, 66);
            this.textBox_tenor.Name = "textBox_tenor";
            this.textBox_tenor.Size = new System.Drawing.Size(100, 24);
            this.textBox_tenor.TabIndex = 9;
            this.textBox_tenor.Text = "1";
            this.textBox_tenor.Leave += new System.EventHandler(this.textBox_tenor_Leave);
            // 
            // textBox_S
            // 
            this.textBox_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_S.Location = new System.Drawing.Point(146, 96);
            this.textBox_S.Name = "textBox_S";
            this.textBox_S.Size = new System.Drawing.Size(100, 24);
            this.textBox_S.TabIndex = 10;
            this.textBox_S.Text = "50";
            this.textBox_S.Leave += new System.EventHandler(this.textBox_S_Leave);
            // 
            // textBox_K
            // 
            this.textBox_K.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_K.Location = new System.Drawing.Point(146, 123);
            this.textBox_K.Name = "textBox_K";
            this.textBox_K.Size = new System.Drawing.Size(100, 24);
            this.textBox_K.TabIndex = 11;
            this.textBox_K.Text = "50";
            this.textBox_K.Leave += new System.EventHandler(this.textBox_K_Leave);
            // 
            // textBox_r
            // 
            this.textBox_r.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_r.Location = new System.Drawing.Point(146, 153);
            this.textBox_r.Name = "textBox_r";
            this.textBox_r.Size = new System.Drawing.Size(100, 24);
            this.textBox_r.TabIndex = 12;
            this.textBox_r.Text = "0.05";
            this.textBox_r.Leave += new System.EventHandler(this.textBox_r_Leave);
            // 
            // textBox_vol
            // 
            this.textBox_vol.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_vol.Location = new System.Drawing.Point(146, 183);
            this.textBox_vol.Name = "textBox_vol";
            this.textBox_vol.Size = new System.Drawing.Size(100, 24);
            this.textBox_vol.TabIndex = 13;
            this.textBox_vol.Text = "0.5";
            this.textBox_vol.Leave += new System.EventHandler(this.textBox_vol_Leave);
            // 
            // button_go
            // 
            this.button_go.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_go.Location = new System.Drawing.Point(106, 620);
            this.button_go.Name = "button_go";
            this.button_go.Size = new System.Drawing.Size(115, 56);
            this.button_go.TabIndex = 16;
            this.button_go.Text = "Go!";
            this.button_go.UseVisualStyleBackColor = true;
            this.button_go.Click += new System.EventHandler(this.button_go_Click);
            // 
            // textBox_price
            // 
            this.textBox_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_price.Location = new System.Drawing.Point(386, 12);
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(207, 24);
            this.textBox_price.TabIndex = 17;
            // 
            // label_price
            // 
            this.label_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_price.Location = new System.Drawing.Point(275, 15);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(42, 18);
            this.label_price.TabIndex = 18;
            this.label_price.Text = "Price";
            // 
            // textBox_delta
            // 
            this.textBox_delta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_delta.Location = new System.Drawing.Point(386, 39);
            this.textBox_delta.Name = "textBox_delta";
            this.textBox_delta.Size = new System.Drawing.Size(207, 24);
            this.textBox_delta.TabIndex = 19;
            // 
            // label_delta
            // 
            this.label_delta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_delta.Location = new System.Drawing.Point(275, 42);
            this.label_delta.Name = "label_delta";
            this.label_delta.Size = new System.Drawing.Size(42, 18);
            this.label_delta.TabIndex = 20;
            this.label_delta.Text = "Delta";
            // 
            // textBox_gamma
            // 
            this.textBox_gamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_gamma.Location = new System.Drawing.Point(386, 66);
            this.textBox_gamma.Name = "textBox_gamma";
            this.textBox_gamma.Size = new System.Drawing.Size(207, 24);
            this.textBox_gamma.TabIndex = 21;
            // 
            // textBox_vega
            // 
            this.textBox_vega.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_vega.Location = new System.Drawing.Point(386, 93);
            this.textBox_vega.Name = "textBox_vega";
            this.textBox_vega.Size = new System.Drawing.Size(207, 24);
            this.textBox_vega.TabIndex = 22;
            // 
            // textBox_theta
            // 
            this.textBox_theta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_theta.Location = new System.Drawing.Point(386, 123);
            this.textBox_theta.Name = "textBox_theta";
            this.textBox_theta.Size = new System.Drawing.Size(207, 24);
            this.textBox_theta.TabIndex = 23;
            // 
            // textBox_rho
            // 
            this.textBox_rho.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_rho.Location = new System.Drawing.Point(386, 153);
            this.textBox_rho.Name = "textBox_rho";
            this.textBox_rho.Size = new System.Drawing.Size(207, 24);
            this.textBox_rho.TabIndex = 24;
            // 
            // textBox_se
            // 
            this.textBox_se.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_se.Location = new System.Drawing.Point(386, 183);
            this.textBox_se.Name = "textBox_se";
            this.textBox_se.Size = new System.Drawing.Size(207, 24);
            this.textBox_se.TabIndex = 25;
            // 
            // label_gamma
            // 
            this.label_gamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_gamma.Location = new System.Drawing.Point(275, 69);
            this.label_gamma.Name = "label_gamma";
            this.label_gamma.Size = new System.Drawing.Size(62, 18);
            this.label_gamma.TabIndex = 26;
            this.label_gamma.Text = "Gamma";
            // 
            // label_vega
            // 
            this.label_vega.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vega.Location = new System.Drawing.Point(275, 96);
            this.label_vega.Name = "label_vega";
            this.label_vega.Size = new System.Drawing.Size(41, 18);
            this.label_vega.TabIndex = 27;
            this.label_vega.Text = "Vega";
            // 
            // label_theta
            // 
            this.label_theta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_theta.Location = new System.Drawing.Point(275, 126);
            this.label_theta.Name = "label_theta";
            this.label_theta.Size = new System.Drawing.Size(45, 18);
            this.label_theta.TabIndex = 28;
            this.label_theta.Text = "Theta";
            // 
            // label_rho
            // 
            this.label_rho.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rho.Location = new System.Drawing.Point(275, 156);
            this.label_rho.Name = "label_rho";
            this.label_rho.Size = new System.Drawing.Size(36, 18);
            this.label_rho.TabIndex = 29;
            this.label_rho.Text = "Rho";
            // 
            // label_se
            // 
            this.label_se.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_se.Location = new System.Drawing.Point(275, 186);
            this.label_se.Name = "label_se";
            this.label_se.Size = new System.Drawing.Size(105, 18);
            this.label_se.TabIndex = 30;
            this.label_se.Text = "Standard Error";
            // 
            // checkBox_antithetic
            // 
            this.checkBox_antithetic.AutoSize = true;
            this.checkBox_antithetic.Location = new System.Drawing.Point(263, 558);
            this.checkBox_antithetic.Name = "checkBox_antithetic";
            this.checkBox_antithetic.Size = new System.Drawing.Size(92, 17);
            this.checkBox_antithetic.TabIndex = 31;
            this.checkBox_antithetic.Text = "Use Antithetic";
            this.checkBox_antithetic.UseVisualStyleBackColor = true;
            // 
            // label_time
            // 
            this.label_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_time.Location = new System.Drawing.Point(275, 216);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(80, 18);
            this.label_time.TabIndex = 32;
            this.label_time.Text = "Time Used";
            // 
            // textBox_time
            // 
            this.textBox_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_time.Location = new System.Drawing.Point(386, 213);
            this.textBox_time.Name = "textBox_time";
            this.textBox_time.Size = new System.Drawing.Size(207, 24);
            this.textBox_time.TabIndex = 33;
            // 
            // checkBox_vc
            // 
            this.checkBox_vc.AutoSize = true;
            this.checkBox_vc.Location = new System.Drawing.Point(386, 558);
            this.checkBox_vc.Name = "checkBox_vc";
            this.checkBox_vc.Size = new System.Drawing.Size(117, 17);
            this.checkBox_vc.TabIndex = 34;
            this.checkBox_vc.Text = "Use Control Variate";
            this.checkBox_vc.UseVisualStyleBackColor = true;
            // 
            // lable_trials
            // 
            this.lable_trials.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_trials.Location = new System.Drawing.Point(12, 15);
            this.lable_trials.Name = "lable_trials";
            this.lable_trials.Size = new System.Drawing.Size(44, 18);
            this.lable_trials.TabIndex = 0;
            this.lable_trials.Text = "Trials";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 698);
            this.progressBar1.Maximum = 13;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(582, 23);
            this.progressBar1.TabIndex = 35;
            // 
            // label_cores
            // 
            this.label_cores.AutoSize = true;
            this.label_cores.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cores.ForeColor = System.Drawing.Color.Red;
            this.label_cores.Location = new System.Drawing.Point(13, 227);
            this.label_cores.Name = "label_cores";
            this.label_cores.Size = new System.Drawing.Size(75, 25);
            this.label_cores.TabIndex = 36;
            this.label_cores.Text = "Cores:";
            // 
            // button_reset
            // 
            this.button_reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_reset.Location = new System.Drawing.Point(386, 620);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(115, 56);
            this.button_reset.TabIndex = 37;
            this.button_reset.Text = "Reset";
            this.button_reset.UseVisualStyleBackColor = true;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // checkBox_MT
            // 
            this.checkBox_MT.AutoSize = true;
            this.checkBox_MT.Location = new System.Drawing.Point(123, 558);
            this.checkBox_MT.Name = "checkBox_MT";
            this.checkBox_MT.Size = new System.Drawing.Size(92, 17);
            this.checkBox_MT.TabIndex = 38;
            this.checkBox_MT.Text = "Multithreading";
            this.checkBox_MT.UseVisualStyleBackColor = true;
            // 
            // label_tenor
            // 
            this.label_tenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tenor.Location = new System.Drawing.Point(13, 69);
            this.label_tenor.Name = "label_tenor";
            this.label_tenor.Size = new System.Drawing.Size(47, 18);
            this.label_tenor.TabIndex = 2;
            this.label_tenor.Text = "Tenor";
            // 
            // radioButton_asian_call
            // 
            this.radioButton_asian_call.Location = new System.Drawing.Point(106, 376);
            this.radioButton_asian_call.Name = "radioButton_asian_call";
            this.radioButton_asian_call.Size = new System.Drawing.Size(42, 17);
            this.radioButton_asian_call.TabIndex = 14;
            this.radioButton_asian_call.Text = "Call";
            this.radioButton_asian_call.UseVisualStyleBackColor = true;
            // 
            // radioButton_asian_put
            // 
            this.radioButton_asian_put.Location = new System.Drawing.Point(205, 376);
            this.radioButton_asian_put.Name = "radioButton_asian_put";
            this.radioButton_asian_put.Size = new System.Drawing.Size(41, 17);
            this.radioButton_asian_put.TabIndex = 15;
            this.radioButton_asian_put.Text = "Put";
            this.radioButton_asian_put.UseVisualStyleBackColor = true;
            // 
            // label_euro
            // 
            this.label_euro.AutoSize = true;
            this.label_euro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_euro.Location = new System.Drawing.Point(103, 284);
            this.label_euro.Name = "label_euro";
            this.label_euro.Size = new System.Drawing.Size(80, 18);
            this.label_euro.TabIndex = 41;
            this.label_euro.Text = "European";
            // 
            // label_asian
            // 
            this.label_asian.AutoSize = true;
            this.label_asian.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_asian.Location = new System.Drawing.Point(103, 355);
            this.label_asian.Name = "label_asian";
            this.label_asian.Size = new System.Drawing.Size(49, 18);
            this.label_asian.TabIndex = 42;
            this.label_asian.Text = "Asian";
            // 
            // radioButton_range
            // 
            this.radioButton_range.Location = new System.Drawing.Point(106, 512);
            this.radioButton_range.Name = "radioButton_range";
            this.radioButton_range.Size = new System.Drawing.Size(62, 19);
            this.radioButton_range.TabIndex = 43;
            this.radioButton_range.Text = "Range";
            this.radioButton_range.UseVisualStyleBackColor = true;
            // 
            // label_range
            // 
            this.label_range.AutoSize = true;
            this.label_range.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_range.Location = new System.Drawing.Point(103, 491);
            this.label_range.Name = "label_range";
            this.label_range.Size = new System.Drawing.Size(56, 18);
            this.label_range.TabIndex = 44;
            this.label_range.Text = "Range";
            // 
            // label_lookback
            // 
            this.label_lookback.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_lookback.Location = new System.Drawing.Point(103, 426);
            this.label_lookback.Name = "label_lookback";
            this.label_lookback.Size = new System.Drawing.Size(93, 17);
            this.label_lookback.TabIndex = 45;
            this.label_lookback.Text = "Loockback";
            // 
            // radioButton_lookback_put
            // 
            this.radioButton_lookback_put.Location = new System.Drawing.Point(204, 446);
            this.radioButton_lookback_put.Name = "radioButton_lookback_put";
            this.radioButton_lookback_put.Size = new System.Drawing.Size(42, 17);
            this.radioButton_lookback_put.TabIndex = 46;
            this.radioButton_lookback_put.Text = "Put";
            this.radioButton_lookback_put.UseVisualStyleBackColor = true;
            // 
            // radioButton_lookback_call
            // 
            this.radioButton_lookback_call.Location = new System.Drawing.Point(106, 446);
            this.radioButton_lookback_call.Name = "radioButton_lookback_call";
            this.radioButton_lookback_call.Size = new System.Drawing.Size(42, 17);
            this.radioButton_lookback_call.TabIndex = 47;
            this.radioButton_lookback_call.Text = "Call";
            this.radioButton_lookback_call.UseVisualStyleBackColor = true;
            // 
            // label_digital
            // 
            this.label_digital.AutoSize = true;
            this.label_digital.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_digital.Location = new System.Drawing.Point(346, 284);
            this.label_digital.Name = "label_digital";
            this.label_digital.Size = new System.Drawing.Size(55, 18);
            this.label_digital.TabIndex = 48;
            this.label_digital.Text = "Digital";
            // 
            // radioButton_digital_call
            // 
            this.radioButton_digital_call.AutoSize = true;
            this.radioButton_digital_call.Location = new System.Drawing.Point(349, 305);
            this.radioButton_digital_call.Name = "radioButton_digital_call";
            this.radioButton_digital_call.Size = new System.Drawing.Size(42, 17);
            this.radioButton_digital_call.TabIndex = 49;
            this.radioButton_digital_call.TabStop = true;
            this.radioButton_digital_call.Text = "Call";
            this.radioButton_digital_call.UseVisualStyleBackColor = true;
            // 
            // radioButton_digital_put
            // 
            this.radioButton_digital_put.AutoSize = true;
            this.radioButton_digital_put.Location = new System.Drawing.Point(439, 305);
            this.radioButton_digital_put.Name = "radioButton_digital_put";
            this.radioButton_digital_put.Size = new System.Drawing.Size(41, 17);
            this.radioButton_digital_put.TabIndex = 50;
            this.radioButton_digital_put.TabStop = true;
            this.radioButton_digital_put.Text = "Put";
            this.radioButton_digital_put.UseVisualStyleBackColor = true;
            // 
            // radioButton_barrier_put
            // 
            this.radioButton_barrier_put.AutoSize = true;
            this.radioButton_barrier_put.Location = new System.Drawing.Point(439, 376);
            this.radioButton_barrier_put.Name = "radioButton_barrier_put";
            this.radioButton_barrier_put.Size = new System.Drawing.Size(41, 17);
            this.radioButton_barrier_put.TabIndex = 55;
            this.radioButton_barrier_put.TabStop = true;
            this.radioButton_barrier_put.Text = "Put";
            this.radioButton_barrier_put.UseVisualStyleBackColor = true;
            // 
            // textBox_barrier
            // 
            this.textBox_barrier.Location = new System.Drawing.Point(408, 355);
            this.textBox_barrier.Name = "textBox_barrier";
            this.textBox_barrier.Size = new System.Drawing.Size(185, 20);
            this.textBox_barrier.TabIndex = 57;
            this.textBox_barrier.Text = "40";
            this.textBox_barrier.Leave += new System.EventHandler(this.textBox_barrier_Leave);
            // 
            // textBox_rebate
            // 
            this.textBox_rebate.Location = new System.Drawing.Point(410, 282);
            this.textBox_rebate.Name = "textBox_rebate";
            this.textBox_rebate.Size = new System.Drawing.Size(183, 20);
            this.textBox_rebate.TabIndex = 58;
            this.textBox_rebate.Text = "5";
            this.textBox_rebate.Leave += new System.EventHandler(this.textBox_rebate_Leave);
            // 
            // radioButton_barrier_up_in
            // 
            this.radioButton_barrier_up_in.AutoSize = true;
            this.radioButton_barrier_up_in.Location = new System.Drawing.Point(3, 73);
            this.radioButton_barrier_up_in.Name = "radioButton_barrier_up_in";
            this.radioButton_barrier_up_in.Size = new System.Drawing.Size(72, 17);
            this.radioButton_barrier_up_in.TabIndex = 51;
            this.radioButton_barrier_up_in.TabStop = true;
            this.radioButton_barrier_up_in.Text = "Up and In";
            this.radioButton_barrier_up_in.UseVisualStyleBackColor = true;
            // 
            // radioButton_barrier_up_out
            // 
            this.radioButton_barrier_up_out.AutoSize = true;
            this.radioButton_barrier_up_out.Location = new System.Drawing.Point(3, 50);
            this.radioButton_barrier_up_out.Name = "radioButton_barrier_up_out";
            this.radioButton_barrier_up_out.Size = new System.Drawing.Size(80, 17);
            this.radioButton_barrier_up_out.TabIndex = 53;
            this.radioButton_barrier_up_out.TabStop = true;
            this.radioButton_barrier_up_out.Text = "Up and Out";
            this.radioButton_barrier_up_out.UseVisualStyleBackColor = true;
            // 
            // radioButton_barrier_down_out
            // 
            this.radioButton_barrier_down_out.AutoSize = true;
            this.radioButton_barrier_down_out.Location = new System.Drawing.Point(3, 3);
            this.radioButton_barrier_down_out.Name = "radioButton_barrier_down_out";
            this.radioButton_barrier_down_out.Size = new System.Drawing.Size(94, 17);
            this.radioButton_barrier_down_out.TabIndex = 54;
            this.radioButton_barrier_down_out.TabStop = true;
            this.radioButton_barrier_down_out.Text = "Down and Out";
            this.radioButton_barrier_down_out.UseVisualStyleBackColor = true;
            // 
            // radioButton_barrier_call
            // 
            this.radioButton_barrier_call.AutoSize = true;
            this.radioButton_barrier_call.Location = new System.Drawing.Point(349, 376);
            this.radioButton_barrier_call.Name = "radioButton_barrier_call";
            this.radioButton_barrier_call.Size = new System.Drawing.Size(42, 17);
            this.radioButton_barrier_call.TabIndex = 56;
            this.radioButton_barrier_call.TabStop = true;
            this.radioButton_barrier_call.Text = "Call";
            this.radioButton_barrier_call.UseVisualStyleBackColor = true;
            // 
            // radioButton_barrier_down_in
            // 
            this.radioButton_barrier_down_in.AutoSize = true;
            this.radioButton_barrier_down_in.Location = new System.Drawing.Point(3, 26);
            this.radioButton_barrier_down_in.Name = "radioButton_barrier_down_in";
            this.radioButton_barrier_down_in.Size = new System.Drawing.Size(86, 17);
            this.radioButton_barrier_down_in.TabIndex = 52;
            this.radioButton_barrier_down_in.TabStop = true;
            this.radioButton_barrier_down_in.Text = "Down and In";
            this.radioButton_barrier_down_in.UseVisualStyleBackColor = true;
            // 
            // label_barrier
            // 
            this.label_barrier.AutoSize = true;
            this.label_barrier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_barrier.Location = new System.Drawing.Point(346, 355);
            this.label_barrier.Name = "label_barrier";
            this.label_barrier.Size = new System.Drawing.Size(59, 18);
            this.label_barrier.TabIndex = 59;
            this.label_barrier.Text = "Barrier";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton_barrier_down_out);
            this.panel1.Controls.Add(this.radioButton_barrier_up_in);
            this.panel1.Controls.Add(this.radioButton_barrier_down_in);
            this.panel1.Controls.Add(this.radioButton_barrier_up_out);
            this.panel1.Location = new System.Drawing.Point(349, 399);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(105, 97);
            this.panel1.TabIndex = 60;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 733);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radioButton_barrier_put);
            this.Controls.Add(this.radioButton_barrier_call);
            this.Controls.Add(this.textBox_barrier);
            this.Controls.Add(this.label_barrier);
            this.Controls.Add(this.radioButton_digital_put);
            this.Controls.Add(this.radioButton_digital_call);
            this.Controls.Add(this.radioButton_euro_put);
            this.Controls.Add(this.radioButton_euro_call);
            this.Controls.Add(this.textBox_rebate);
            this.Controls.Add(this.label_euro);
            this.Controls.Add(this.label_digital);
            this.Controls.Add(this.radioButton_asian_put);
            this.Controls.Add(this.radioButton_asian_call);
            this.Controls.Add(this.radioButton_lookback_put);
            this.Controls.Add(this.radioButton_lookback_call);
            this.Controls.Add(this.label_asian);
            this.Controls.Add(this.label_range);
            this.Controls.Add(this.label_lookback);
            this.Controls.Add(this.radioButton_range);
            this.Controls.Add(this.checkBox_MT);
            this.Controls.Add(this.button_reset);
            this.Controls.Add(this.label_cores);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lable_trials);
            this.Controls.Add(this.checkBox_vc);
            this.Controls.Add(this.textBox_time);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.checkBox_antithetic);
            this.Controls.Add(this.label_se);
            this.Controls.Add(this.label_rho);
            this.Controls.Add(this.label_theta);
            this.Controls.Add(this.label_vega);
            this.Controls.Add(this.label_gamma);
            this.Controls.Add(this.textBox_se);
            this.Controls.Add(this.textBox_rho);
            this.Controls.Add(this.textBox_theta);
            this.Controls.Add(this.textBox_vega);
            this.Controls.Add(this.textBox_gamma);
            this.Controls.Add(this.label_delta);
            this.Controls.Add(this.textBox_delta);
            this.Controls.Add(this.label_price);
            this.Controls.Add(this.textBox_price);
            this.Controls.Add(this.button_go);
            this.Controls.Add(this.textBox_vol);
            this.Controls.Add(this.textBox_r);
            this.Controls.Add(this.textBox_K);
            this.Controls.Add(this.textBox_S);
            this.Controls.Add(this.textBox_tenor);
            this.Controls.Add(this.textBox_steps);
            this.Controls.Add(this.textBox_trials);
            this.Controls.Add(this.label_vol);
            this.Controls.Add(this.label_r);
            this.Controls.Add(this.label_K);
            this.Controls.Add(this.label_S);
            this.Controls.Add(this.label_tenor);
            this.Controls.Add(this.lable_steps);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Monte Carlo Simulation";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lable_steps;
        private System.Windows.Forms.Label label_S;
        private System.Windows.Forms.Label label_K;
        private System.Windows.Forms.Label label_r;
        private System.Windows.Forms.Label label_vol;
        private System.Windows.Forms.TextBox textBox_trials;
        private System.Windows.Forms.TextBox textBox_steps;
        private System.Windows.Forms.TextBox textBox_tenor;
        private System.Windows.Forms.TextBox textBox_S;
        private System.Windows.Forms.TextBox textBox_K;
        private System.Windows.Forms.TextBox textBox_r;
        private System.Windows.Forms.TextBox textBox_vol;
        private System.Windows.Forms.RadioButton radioButton_euro_call;
        private System.Windows.Forms.RadioButton radioButton_euro_put;
        private System.Windows.Forms.Button button_go;
        private System.Windows.Forms.TextBox textBox_price;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.TextBox textBox_delta;
        private System.Windows.Forms.Label label_delta;
        private System.Windows.Forms.TextBox textBox_gamma;
        private System.Windows.Forms.TextBox textBox_vega;
        private System.Windows.Forms.TextBox textBox_theta;
        private System.Windows.Forms.TextBox textBox_rho;
        private System.Windows.Forms.TextBox textBox_se;
        private System.Windows.Forms.Label label_gamma;
        private System.Windows.Forms.Label label_vega;
        private System.Windows.Forms.Label label_theta;
        private System.Windows.Forms.Label label_rho;
        private System.Windows.Forms.Label label_se;
        private System.Windows.Forms.CheckBox checkBox_antithetic;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.TextBox textBox_time;
        private System.Windows.Forms.CheckBox checkBox_vc;
        private System.Windows.Forms.Label lable_trials;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label_cores;
        private System.Windows.Forms.Button button_reset;
        private System.Windows.Forms.CheckBox checkBox_MT;
        private System.Windows.Forms.Label label_tenor;
        private System.Windows.Forms.Label label_euro;
        private System.Windows.Forms.Label label_asian;
        private System.Windows.Forms.RadioButton radioButton_asian_call;
        private System.Windows.Forms.RadioButton radioButton_asian_put;
        private System.Windows.Forms.RadioButton radioButton_range;
        private System.Windows.Forms.Label label_range;
        private System.Windows.Forms.Label label_lookback;
        private System.Windows.Forms.RadioButton radioButton_lookback_put;
        private System.Windows.Forms.RadioButton radioButton_lookback_call;
        private System.Windows.Forms.Label label_digital;
        private System.Windows.Forms.RadioButton radioButton_digital_call;
        private System.Windows.Forms.RadioButton radioButton_digital_put;
        private System.Windows.Forms.RadioButton radioButton_barrier_put;
        private System.Windows.Forms.TextBox textBox_barrier;
        private System.Windows.Forms.TextBox textBox_rebate;
        private System.Windows.Forms.Label label_barrier;
        private System.Windows.Forms.RadioButton radioButton_barrier_down_in;
        private System.Windows.Forms.RadioButton radioButton_barrier_call;
        private System.Windows.Forms.RadioButton radioButton_barrier_down_out;
        private System.Windows.Forms.RadioButton radioButton_barrier_up_out;
        private System.Windows.Forms.RadioButton radioButton_barrier_up_in;
        private System.Windows.Forms.Panel panel1;
    }
}

