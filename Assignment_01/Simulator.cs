﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Assignment_01
{
    //this class generates random numbers and paths
    public class Simulator
    {

        //Instan. random class
        Random rnd = new Random();

        //When Simulator.random is called, it returns a random matrix based on user inputs
        public double[,] random()
        {

            //create an empty matrix
            double[,] r = new double[IO.Trials, IO.Steps];

            //For efficiency purpose, if the number of trials is even, go to the 1st for loop, if it is odd, then go to the 2nd for loop
            if (IO.Trials % 2 == 0)
            {
                //Loop calculation to generate random matrix with even number of trials, each workload generate 2 rows of random numbers
                Parallel.ForEach(Ienum.Step(0, IO.Trials, 2), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    for (long b = 0; b < IO.Steps; b++)
                    {
                        double[,] randn = new double[1, 2];
                        randn = rand_2();
                        r[a, b] = randn[0, 0];
                        r[a + 1, b] = randn[0, 1];
                    }
                });
            }
            else//If the number of trails if odd, the last row of the matrix is solely generated, and the rest rows are generated in the same way as the even one.
            {
                Parallel.ForEach(Ienum.Step(0, IO.Trials-1, 2), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    for (long b = 0; b < IO.Steps; b++)
                    {
                        double[,] randn = new double[1, 2];
                        randn = rand_2();
                        r[a, b] = randn[0, 0];
                        r[a + 1, b] = randn[0, 1];
                    }
                });
                for (long b = 0; b < IO.Steps; b++)
                {
                    r[IO.Trials - 1, b] = rand_1();
                }
            }
            return r;
        }
        //this private method generates ONE random number at a time.



        public double[,] Getpath(double[,] r, int Trials, int Steps, double Tenor, double S, double R, double vol,double Rebate,double Barrier)
        {
            if(IO.Antithetic==true)
            { //generate antithetic path
                double[,] path = new double[2 * Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);
                for (int a = 0; a <= (2*Trials) - 1; a++)
                {
                    path[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        path[a, b] = path[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                        path[(2 * Trials) - 1 - a, b] = path[(2 * Trials) - 1 - a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) - (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                return path;
            }
            else
            {//generate normal path
                double[,] path = new double[Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= Trials - 1; a++)
                {
                    path[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                         path[a, b] = path[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                return path;
            }
        }
      
        private double rand_1()
        {
            var obj = new Object();
            double randn1 = 0, randn2 = 0, w = 0, c = 0;
            //for parallel computing in the future, lock to ensure serial access
            do
            {
                lock (rnd) randn1 = 2*rnd.NextDouble()-1;
                lock (rnd) randn2 = 2*rnd.NextDouble()-1;
                w = (randn1 * randn1) + (randn2 * randn2);
            }
            while (w > 1);
            c = Math.Sqrt(-2 * Math.Log(w) / w);
            double z1 = 0, z2 = 0;
            z1 = c * randn1;
            z2 = c * randn2;
            return z1;
        }

        //this private method generates TWO random numbers at a time.
        private double[,] rand_2()
        {
            var obj = new Object();
            double randn1 = 0, randn2 = 0, w = 0, c = 0;
            do
            {
                lock (rnd) randn1 = 2 * rnd.NextDouble() - 1;
                lock (rnd) randn2 = 2 * rnd.NextDouble() - 1;
                w = (randn1 * randn1) + (randn2 * randn2);
            }
            while (w > 1);
            c = Math.Sqrt(-2 * Math.Log(w) / w);
            double[,] numb_2 = new double[1, 2];
            numb_2[0, 0] = c * randn1;
            numb_2[0, 1] = c * randn2;
            return numb_2;
        }

    }
}
