﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Assignment_01
{
    public partial class Form1 : Form
    {
        //input check flags
        bool flag_trials = false;
        bool flag_steps = false;
        bool flag_tenor = false;
        bool flag_S = false;
        bool flag_K = false;
        bool flag_r = false;
        bool flag_vol = false;
        bool flag_rebate = false;
        bool flag_barrier = false;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            IO.Cpu_cores = System.Environment.ProcessorCount;
            label_cores.Text = "Cores:" + Convert.ToString(IO.Cpu_cores);
            progressBar1.Value = 0;
         
        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            //Form1_Load(sender, e);
            IO.Cpu_cores = System.Environment.ProcessorCount;
            label_cores.Text = "Cores:" + Convert.ToString(IO.Cpu_cores);
            progressBar1.Value = 0;
            textBox_trials.Text = "";
            textBox_steps.Text = "";
            textBox_tenor.Text = "";
            textBox_S.Text = "";
            textBox_K.Text = "";
            textBox_r.Text = "";
            textBox_vol.Text = "";
            textBox_barrier.Text = "";
            textBox_rebate.Text = "";
            textBox_price.Text = "";
            textBox_delta.Text = "";
            textBox_gamma.Text = "";
            textBox_vega.Text = "";
            textBox_theta.Text = "";
            textBox_rho.Text = "";
            textBox_se.Text = "";
            textBox_time.Text = "";
        }

        public void finish()
        {
            string price = Convert.ToString(IO.O_price);
            string delta = Convert.ToString(IO.O_delta);
            string gamma = Convert.ToString(IO.O_gamma);
            string vega = Convert.ToString(IO.O_vega);
            string theta = Convert.ToString(IO.O_theta);
            string rho = Convert.ToString(IO.O_rho);
            string se = Convert.ToString(IO.O_se);
            textBox_price.Text = price;
            textBox_delta.Text = delta;
            textBox_gamma.Text = gamma;
            textBox_vega.Text = vega;
            textBox_theta.Text = theta;
            textBox_rho.Text = rho;
            textBox_se.Text = se;
            textBox_time.Text = IO.Timer;

        }
        private void button_go_Click(object sender, EventArgs e)
        {
            //Instan. all option classes
            Option EU = new Option();
            Asian Asian = new Asian();
            Range Range = new Range();
            Lookback LB = new Lookback();
            Digital Dig = new Digital();
            Barrier Ba = new Barrier();


            progressBar1.Value = 0;
            //when button_go is clicked, check inputs
            textBox_trials_Leave(sender, e);
            textBox_steps_Leave(sender, e);
            textBox_tenor_Leave(sender, e);
            textBox_S_Leave(sender, e);
            textBox_K_Leave(sender, e);
            textBox_r_Leave(sender, e);
            textBox_vol_Leave(sender, e);
            textBox_rebate_Leave(sender, e);
            textBox_barrier_Leave(sender, e);


            if (flag_trials && flag_steps && flag_tenor && flag_S && flag_K && flag_r && flag_vol)
            {

                //Transfer inputs to IO class
                IO.Steps = Convert.ToInt32(textBox_steps.Text);
                IO.Trials = Convert.ToInt32(textBox_trials.Text);
                IO.Tenor = Convert.ToDouble(textBox_tenor.Text);
                IO.Underlying = Convert.ToDouble(textBox_S.Text);
                IO.Strike_Price = Convert.ToDouble(textBox_K.Text);
                IO.Risk_free_rate = Convert.ToDouble(textBox_r.Text);
                IO.Volatility = Convert.ToDouble(textBox_vol.Text);
                IO.Rebate = Convert.ToDouble(textBox_rebate.Text);
                IO.Barrier = Convert.ToDouble(textBox_barrier.Text);
                if (radioButton_euro_call.Checked || radioButton_asian_call.Checked||radioButton_lookback_call.Checked||radioButton_digital_call.Checked||radioButton_barrier_call.Checked)
                {
                    IO.Call = true;
                }
                else if (radioButton_euro_put.Checked || radioButton_asian_put.Checked||radioButton_lookback_put.Checked||radioButton_digital_put.Checked||radioButton_barrier_put.Checked)
                {
                    IO.Call = false;
                }
                else if (radioButton_range.Checked)
                {
                    IO.Call = true;
                }
               
                if(radioButton_barrier_up_in.Checked)
                {
                    IO.UP = true;
                    IO.IN = true;
                }
                if (radioButton_barrier_up_out.Checked)
                {
                    IO.UP = true;
                    IO.IN = false;
                }
                if (radioButton_barrier_down_in.Checked)
                {
                    IO.UP = false;
                    IO.IN = true;
                }
                if (radioButton_barrier_down_out.Checked)
                {
                    IO.UP = false;
                    IO.IN = false;
                }

                if (checkBox_vc.Checked)
                {
                    IO.CV = true;
                }
                else
                {
                    IO.CV = false;
                }

                if (checkBox_antithetic.Checked)
                {
                    IO.Antithetic = true;
                }
                else
                {
                    IO.Antithetic = false;
                }
                if (checkBox_MT.Checked)
                {
                    IO.Thread = IO.Cpu_cores;
                }
                else
                {
                    IO.Thread = 1;
                }
             
                //Based on user's choice, run differenct class
                //For call and put radiobuttom of each option, user is only allowed to choose one at a time 
                if (radioButton_euro_call.Checked || radioButton_euro_put.Checked)
                {
                    Thread C = new Thread(new ThreadStart(EU.schedular));
                    C.Start();
                }
                else if(radioButton_asian_call.Checked || radioButton_asian_put.Checked)
                {
                    Thread C = new Thread(new ThreadStart(Asian.schedular));
                    C.Start();
                }
                else if (radioButton_range.Checked)
                {
                    Thread C = new Thread(new ThreadStart(Range.schedular));
                    C.Start();
                }
                else if (radioButton_lookback_call.Checked || radioButton_lookback_put.Checked)
                {
                    Thread C = new Thread(new ThreadStart(LB.schedular));
                    C.Start();
                }
                else if (radioButton_digital_call.Checked || radioButton_digital_put.Checked)
                {
                    Thread C = new Thread(new ThreadStart(Dig.schedular));
                    C.Start();
                }
                else if(radioButton_barrier_call.Checked||radioButton_barrier_put.Checked)
                {
                    Thread C = new Thread(new ThreadStart(Ba.schedular));
                    C.Start();
                }
            }
            else
            {
                MessageBox.Show("Please check your inputs");
            }
        
        }










        //====================The following part is error checking of each textbox==========================
        private void textBox_trials_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_trials.Text, out check))
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_trials.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_trials.Text) < 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_trials.Text = "Wrong";
            }
            //check if it is an integer
            else if (Convert.ToDouble(textBox_trials.Text) % 1 != 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_trials.Text = "Wrong";
            }
            else//if all true, then flag goes to true
            {
                flag_trials = true;
            }
        }

        private void textBox_steps_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number of not
            if (!double.TryParse(textBox_steps.Text, out check))
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_steps.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_steps.Text) < 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_steps.Text = "Wrong";
            }
            //check if it is an integer
            else if (Convert.ToDouble(textBox_steps.Text) % 1 != 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_steps.Text = "Wrong";
            }
            else//if all true, then flag goes to true
            {
                flag_steps = true;
            }
        }

        private void textBox_tenor_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_tenor.Text, out check))
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_tenor.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_tenor.Text) < 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_tenor.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_tenor = true;
            }
        }

        private void textBox_S_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_S.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_S.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_S.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_S.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_S = true;
            }
        }

        private void textBox_K_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_K.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_K.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_K.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_K.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_K = true;
            }
        }

        private void textBox_r_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_r.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_r.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_r.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_r.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_r = true;
            }
        }

        private void textBox_vol_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_vol.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_vol.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_vol.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_vol.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_vol = true;
            }
        }
        private void textBox_rebate_Leave(object sender, EventArgs e)
        {
            double check;
            if (!double.TryParse(textBox_rebate.Text, out check))
            {
                //check if is a number
                MessageBox.Show("Please enter a number >0");
                textBox_rebate.Text = "Wrong";
            }
            //check it is positive
            else if (Convert.ToDouble(textBox_rebate.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_rebate.Text = "Wrong";
            }
            else
            {//if all true, flag goegs to true
                flag_rebate = true;
            }
        }
        private void textBox_barrier_Leave(object sender, EventArgs e)
        {
            double check;
            if (!double.TryParse(textBox_barrier.Text, out check))
            {
                //check if it a number
                MessageBox.Show("Please enter a barrier larger than S for Up option, and enter a rebate smaller than S for Down option");
                textBox_barrier.Text = "Wrong";
            }
            else if (Convert.ToDouble(textBox_barrier.Text) < 0)
            {
                //check if it positive
                MessageBox.Show("Please enter a number >0");
                textBox_barrier.Text = "Wrong";
            }
            else if (Convert.ToDouble(textBox_barrier.Text) > 0)
            {
                if (radioButton_barrier_down_in.Checked || radioButton_barrier_down_out.Checked)
                {
                    //for down barrier, check if it is smaller than S
                    if (Convert.ToDouble(textBox_barrier.Text) >= Convert.ToDouble(textBox_S.Text))
                    {
                        MessageBox.Show("Please enter a barrier larger than S for Up option, and enter a rebate smaller than S for Down option");
                        textBox_barrier.Text = "Wrong";
                    }
                }
                else if (radioButton_barrier_up_in.Checked || radioButton_barrier_up_out.Checked)
                {
                    //for up barrier, check if it is larger than S
                    if (Convert.ToDouble(textBox_barrier.Text) <= Convert.ToDouble(textBox_S.Text))
                    {
                        MessageBox.Show("Please enter a barrier larger than S for Up option, and enter a rebate smaller than S for Down option");
                        textBox_barrier.Text = "Wrong";
                    }
                }
            }
            else
            {//all true, set flag as true
                flag_barrier = true;
            }
        }

        
    }
}
