﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    class Asian1:Option
    {
        public override double[,] Getprice(double[,] Path, int Trials, int Steps, double Tenor, double S, double K, double R, double vol)
        {
            if (IO.Antithetic == true)
            {
                if (IO.CV == true)
                {
                    if (IO.Call == true)
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double[,] average = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            average[a, 0] = Path[a, 0];
                            average[(2 * Trials) - 1 - a, 0] = Path[(2 * Trials) - 1 - a, 0];
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));

                                average[a, 0] += Path[a, b];
                                average[(2 * Trials) - 1 - a, 0] += Path[(2*Trials)-1-a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                            average[(2 * Trials) - 1 - a, 0] = average[(2 * Trials) - 1 - a, 0] / Convert.ToDouble(Steps + 1);
                            CT[a, 0] = 0.5 * (Math.Max(average[a, 0] - K, 0) - cv[a, 0] + Math.Max(average[(2 * Trials) - 1 - a, 0] - K, 0) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                    else
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double[,] average = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            average[a, 0] = Path[a, 0];
                            average[(2 * Trials) - 1 - a, 0] = Path[(2 * Trials) - 1 - a, 0];
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));

                                average[a, 0] += Path[a, b];
                                average[(2 * Trials) - 1 - a, 0] += Path[(2*Trials)-1-a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                            average[(2 * Trials) - 1 - a, 0] = average[(2 * Trials) - 1 - a, 0] / Convert.ToDouble(Steps + 1);
                            CT[a, 0] = 0.5 * (Math.Max(K-average[a, 0], 0) - cv[a, 0] + Math.Max(K-average[(2 * Trials) - 1 - a, 0], 0) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                }
                else
                {
                    if (IO.Call == true)
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        double[,] average = new double[2 * Trials, 1];

                        for(int a = 0; a <= Trials - 1; a++)
                        {
                            average[a, 0] = Path[a, 0];
                            average[(2 * Trials) - 1 - a, 0] = Path[(2 * Trials) - 1 - a, 0];
                            for(int b = 1; b <= Steps; b++)
                            {
                                average[a, 0] += Path[a, b];
                                average[(2 * Trials) - 1 - a, 0] += Path[(2 * Trials) - 1 - a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                            average[(2 * Trials) - 1 - a, 0] = average[(2 * Trials) - 1 - a, 0] / Convert.ToDouble(Steps + 1);
                        }
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(average[a, 0] - K, 0);

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(average[a - 1, 0] - K, 0) * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                    else
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        double[,] average = new double[2 * Trials, 1];

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            average[a, 0] = Path[a, 0];
                            average[(2 * Trials) - 1 - a, 0] = Path[(2 * Trials) - 1 - a, 0];
                            for (int b = 1; b <= Steps; b++)
                            {
                                average[a, 0] += Path[a, b];
                                average[(2 * Trials) - 1 - a, 0] += Path[(2 * Trials) - 1 - a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                            average[(2 * Trials) - 1 - a, 0] = average[(2 * Trials) - 1 - a, 0] / Convert.ToDouble(Steps + 1);
                        }
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(K-average[a, 0], 0);

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(K-average[a - 1, 0], 0) * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                }
            }
            else
            {
                if (IO.CV == true)
                {
                    if (IO.Call == true)
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double[,] average = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            average[a, 0] = Path[a, 0];
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                average[a, 0] += average[a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                            CT[a, 0] = Math.Max(average[a, 0] - K, 0) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }
                    else
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double[,] average = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            average[a, 0] = Path[a, 0];
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                average[a, 0] += average[a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                            CT[a, 0] = Math.Max(K - average[a, 0], 0) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }
                }
                else
                {
                    if (IO.Call == true)
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        double[,] average = new double[Trials, 1];

                        for(int a = 0; a <= Trials - 1; a++)
                        {
                            for(int b = 0; b <= Steps; b++)
                            {
                                average[a, 0] += Path[a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                        }
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(average[a, 0] - K, 0);

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(average[a - 1, 0] - K, 0) * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                    else
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        double[,] average = new double[Trials, 1];

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 0; b <= Steps; b++)
                            {
                                average[a, 0] += Path[a, b];
                            }
                            average[a, 0] = average[a, 0] / Convert.ToDouble(Steps + 1);
                        }
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(K-average[a, 0], 0);

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(K-average[a - 1, 0], 0) * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                }
            }




            //return base.Getprice(Path, Trials, Steps, Tenor, S, K, R, vol);
        }
    }
}
