﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Assignment_01
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //initialize new thread to start GUI
            Thread A = new Thread(RunGUI);
            A.Start();//start the thread and join
            A.Join();
           
        }

        public static Form1 GUI = new Form1();

        delegate void progresscheck(int amount);
        delegate void finish();

        static void RunGUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(GUI);
        }
        //this method increases progress bar when called
        //also it checked if invoke is required
        public static void increase(int input)
        {
            //check whether invoke is required
            if (Program.GUI.progressBar1.InvokeRequired)
            {
                //if required, begin invoke and increase bar
                progresscheck progressdelegate = new progresscheck(progress_check_method);
                GUI.progressBar1.BeginInvoke(progressdelegate, input);
            }
            else
            {
                //if not, just increase the bar
                progress_check_method(input);
            }
        }

        //this method transfer all the results into IO class
        public static void finished()
        {
            if(Program.GUI.progressBar1.InvokeRequired)
            {
                finish finishdelegate = new finish(finish_check_method);
                GUI.progressBar1.BeginInvoke(finishdelegate);
            }
            else
            {
                finish_check_method();
            }
            
        }

        public static void progress_check_method(int amount)
        {
            GUI.progressBar1.Value += amount;
            GUI.progressBar1.Update();
        }

        public static void finish_check_method()
        {
            GUI.finish();
        }


    }
}
