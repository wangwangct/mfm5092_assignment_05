﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    class Barrier:Option
    {
        public override double[,] Getprice(double[,] Path, int Trials, int Steps, double Tenor, double S, double K, double R, double vol, double Rebate, double Barrier)
        {
            //Barrier option is somehow similar to digital option, first we need to decide whether a path is in the money or not, and then we calculate payoff
            //The same logic is used in Barrier option. We still use an array, 1 is in the money, 0 is out the money. 
            //If it is in the money, we do pricing as pricing European option. If it is out the money, then its payoff is 0



            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================
            if (IO.Antithetic == true)//with antithetic
            {

                //===========================================================================
                //Decide in or out the money
                //===========================================================================
                double[] In_the_money = new double[2 * Trials];
                if (IO.UP == true & IO.IN == true)//Up and In
                {
                    Parallel.ForEach(Ienum.Step(0, (2 * Trials) - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] > Barrier)
                            {
                                In_the_money[a] = 1;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                    });

                }
                else if (IO.UP == true & IO.IN == false)//Up and Out
                {
                    Parallel.ForEach(Ienum.Step(0, (2 * Trials) - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] > Barrier)
                            {
                                In_the_money[a] = 0;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 1;
                            }
                        }
                    });
                }
                else if (IO.UP == false & IO.IN == true)//Down and In
                {
                    Parallel.ForEach(Ienum.Step(0, (2 * Trials) - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] < Barrier)
                            {
                                In_the_money[a] = 1;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                    });
                }
                else//(IO.UP==false&IO.IN==false)//Down and Out
                {
                    Parallel.ForEach(Ienum.Step(0, (2 * Trials) - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] < Barrier)
                            {
                                In_the_money[a] = 0;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 1;
                            }
                        }
                    });
                }
                //===========================================================================
                //Decide in or out the money
                //===========================================================================
                if (IO.CV == true)
                {
                    if (IO.Call == true)//anti&CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * ((Math.Max(Path[a, Steps] - K, 0)*In_the_money[a]) - cv[a, 0] + (Math.Max(Path[(2 * Trials) - 1 - a, Steps] - K, 0)*In_the_money[(2*Trials)-1-a]) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                    else//anti&CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[2 * Trials, 1];
                        double delta_1, delta_2;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                            cv[(2 * Trials) - 1 - a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                delta_2 = BS.Delta(Path[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                                cv[(2 * Trials) - 1 - a, 0] += delta_2 * (Path[(2 * Trials) - 1 - a, b] - (Path[(2 * Trials) - 1 - a, b - 1] * erddt));
                            }
                            CT[a, 0] = 0.5 * ((Math.Max(K-Path[a, Steps], 0) * In_the_money[a]) - cv[a, 0] + (Math.Max(K-Path[(2 * Trials) - 1 - a, Steps], 0) * In_the_money[(2 * Trials) - 1 - a]) - cv[(2 * Trials) - 1 - a, 0]);
                        }
                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        return sum_ct;
                    }
                }
                else
                {
                    if (IO.Call == true)//anti&call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(Path[a, Steps] - K, 0)*In_the_money[a];

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(Path[a - 1, Steps] - K, 0)*In_the_money[a-1] * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                    else//anti&put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, (2 * Trials) + 1];
                        for (int a = 0; a <= (2 * Trials) - 1; a++)
                        {
                            sum += Math.Max(K-Path[a, Steps], 0) * In_the_money[a];

                        }
                        Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= (2 * Trials); a++)
                        {
                            Result[0, a] = Math.Max(K-Path[a - 1, Steps], 0) * In_the_money[a - 1] * Math.Exp(-(R * Tenor));
                        }
                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/ Antithetic
            //==============================================================================================================================================





            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================
            else//without antithetic
            {
                //===========================================================================
                //Decide in or out the money
                //===========================================================================
                double[] In_the_money = new double[Trials];
                if (IO.UP == true & IO.IN == true)//Up and In
                {
                    Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] > Barrier)
                            {
                                In_the_money[a] = 1;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                    });

                }
                else if (IO.UP == true & IO.IN == false)//Up and Out
                {
                    Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] > Barrier)
                            {
                                In_the_money[a] = 0;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 1;
                            }
                        }
                    });
                }
                else if (IO.UP == false & IO.IN == true)//Down and In
                {
                    Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] < Barrier)
                            {
                                In_the_money[a] = 1;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 0;
                            }
                        }
                    });
                }
                else//(IO.UP==false&IO.IN==false)//Down and Out
                {
                    Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                    {
                        for (int b = 1; b <= Steps; b++)
                        {
                            if (Path[a, b] < Barrier)
                            {
                                In_the_money[a] = 0;
                                break;
                            }
                            else
                            {
                                In_the_money[a] = 1;
                            }
                        }
                    });
                }
                //===========================================================================
                //Decide in or out the money
                //===========================================================================
                if (IO.CV == true)
                {
                    if (IO.Call == true)//CV&call
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = (Math.Max(Path[a, Steps] - K, 0)*In_the_money[a]) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }
                    else//CV&put
                    {
                        double[,] CT = new double[Trials, 1];
                        double dt = Tenor / Convert.ToDouble(Steps);
                        double[,] cv = new double[Trials, 1];
                        double delta_1;
                        double cont_t;
                        double erddt = Math.Exp(R * dt);

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            cv[a, 0] = 0;
                        }

                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            for (int b = 1; b <= Steps; b++)
                            {
                                cont_t = (b - 1) * dt;
                                delta_1 = BS.Delta(Path[a, b - 1], cont_t, Tenor, K, vol, R);
                                //calculate cv
                                cv[a, 0] += delta_1 * (Path[a, b] - (Path[a, b - 1] * erddt));
                            }
                            CT[a, 0] = (Math.Max(K-Path[a, Steps], 0) * In_the_money[a]) - cv[a, 0];
                        }

                        //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                        double[,] sum_ct = new double[1, 3];
                        sum_ct[0, 0] = 0;
                        sum_ct[0, 1] = 0;
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum_ct[0, 0] += CT[a, 0];
                            sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                        }
                        sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                        return sum_ct;
                    }

                }
                else
                {
                    if (IO.Call == true)//call
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(Path[a, Steps] - K, 0)*In_the_money[a];

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(Path[a - 1, Steps] - K, 0)*In_the_money[a-1] * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                    else//put
                    {
                        double sum = 0;
                        double[,] Result = new double[1, Trials + 1];
                        for (int a = 0; a <= Trials - 1; a++)
                        {
                            sum += Math.Max(K-Path[a, Steps], 0) * In_the_money[a];

                        }

                        Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                        for (int a = 1; a <= Trials; a++)
                        {
                            Result[0, a] = Math.Max(K-Path[a - 1, Steps], 0) * In_the_money[a-1] * Math.Exp(-(R * Tenor));
                        }

                        return Result;
                    }
                }
            }
            //==============================================================================================================================================
            //Pricing W/O Antithetic
            //==============================================================================================================================================
        }
    }
}
